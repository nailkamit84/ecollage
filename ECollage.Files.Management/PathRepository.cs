﻿using ECollage.Localization.Enum;

namespace ECollage.Files.Management
{
    public class PathRepository
    {
        public static string[] ClientAttachments(FileType fileType)
        {

            return new string[]
            {
                    "EColWebSite",//MasterISS
                    "Client Attachments",
                    fileType.ToString(),
            };

        }
    }
}
