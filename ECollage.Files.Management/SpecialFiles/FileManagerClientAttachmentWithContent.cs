﻿using System.IO;

namespace ECollage.Files.Management.SpecialFiles
{
    public class FileManagerClientAttachmentWithContent : FileManagerSpecialFileWithContent<FileManagerClientAttachment>
    {
        public FileManagerClientAttachmentWithContent(Stream content, FileManagerClientAttachment fileDetails) : base(content, fileDetails) { }
    }
}
