﻿using ECollage.Files.Concrete;
using ECollage.Files.Management.SpecialFiles;
using ECollage.Localization.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Files.Management
{
    public partial class EColFileManager
    {
        private string GetClientAttachmentsPath(long id, FileType fileType)
        {
            var resultPathParts = PathRepository.ClientAttachments(fileType).Concat(GetIdPathPartition(id));
            var resulPath = string.Join(InternalFileManager.PathSeparator, resultPathParts);

            return resulPath;
        }

        public FileManagerResult<IEnumerable<FileManagerClientAttachment>> GetClientAttachmentsList(long id, FileType fileType)
        {
            var searchPath = GetClientAttachmentsPath(id, fileType);
            InternalFileManager.GoToRootDirectory();
            var result = InternalFileManager.EnterDirectoryPath(searchPath);
            if (!result.Result)
            {
                return new FileManagerResult<IEnumerable<FileManagerClientAttachment>>(Enumerable.Empty<FileManagerClientAttachment>(), result.InternalException);
            }
            var fileListResult = InternalFileManager.GetFileList();
            if (fileListResult.InternalException != null)
            {
                return new FileManagerResult<IEnumerable<FileManagerClientAttachment>>(null, fileListResult.InternalException);
            }
            else if (fileListResult.Result != null)
            {
                return new FileManagerResult<IEnumerable<FileManagerClientAttachment>>(fileListResult.Result.Select(fileName => new FileManagerClientAttachment(fileName)));
            }
            return new FileManagerResult<IEnumerable<FileManagerClientAttachment>>(Enumerable.Empty<FileManagerClientAttachment>());
        }

        public FileManagerResult<FileManagerResultEnum> SaveClientAttachment(object categoryIdResultLong, FileManagerClientAttachmentWithContent newFile, FileType product)
        {
            throw new NotImplementedException();
        }

        public FileManagerResult<FileManagerResultEnum> SaveClientAttachment(long id, FileManagerClientAttachmentWithContent attachment, FileType fileType)
        {
            var destinationPath = GetClientAttachmentsPath(id, fileType);
            InternalFileManager.GoToRootDirectory();
            var result = CreateAndEnterPath(destinationPath);
            if (!result.Result)
            {
                return new FileManagerResult<FileManagerResultEnum>(FileManagerResultEnum.Error);
            }
            // check file hash to prevent dupes
            var listResult = InternalFileManager.GetFileList();
            if (listResult.InternalException != null)
            {
                return new FileManagerResult<FileManagerResultEnum>(listResult.InternalException);
            }
            if (listResult.Result != null && listResult.Result.Any(fileName => fileName.Contains($".{attachment.FileDetail.MD5}")))
            {
                return new FileManagerResult<FileManagerResultEnum>(FileManagerResultEnum.AlreadyExists);
            }
            // save file
            result = InternalFileManager.SaveFile(attachment.FileDetail.ServerSideName, attachment.Content);
            if (!result.Result)
            {
                return new FileManagerResult<FileManagerResultEnum>(FileManagerResultEnum.Error);
            }
            return new FileManagerResult<FileManagerResultEnum>(FileManagerResultEnum.Success);
        }

        public FileManagerResult<FileManagerClientAttachmentWithContent> GetClientAttachment(long id, string fileName, FileType fileType)
        {
            var destinationPath = GetClientAttachmentsPath(id, fileType);
            InternalFileManager.GoToRootDirectory();
            var result = InternalFileManager.EnterDirectoryPath(destinationPath);
            if (result.InternalException != null)
            {
                return new FileManagerResult<FileManagerClientAttachmentWithContent>(result.InternalException);
            }
            else if (!result.Result)
            {
                return new FileManagerResult<FileManagerClientAttachmentWithContent>(new InvalidOperationException("File not found!"));
            }

            var fileResult = InternalFileManager.GetFile(fileName);
            if (fileResult.InternalException != null)
            {
                return new FileManagerResult<FileManagerClientAttachmentWithContent>(fileResult.InternalException);
            }

            return new FileManagerResult<FileManagerClientAttachmentWithContent>(new FileManagerClientAttachmentWithContent(fileResult.Result, new FileManagerClientAttachment(fileName)));
        }

        public FileManagerResult<bool> RemoveClientAttachment(long id, string fileName, FileType fileType)
        {
            var destinationPath = GetClientAttachmentsPath(id, fileType);
            InternalFileManager.GoToRootDirectory();
            var result = InternalFileManager.EnterDirectoryPath(destinationPath);
            if (result.InternalException != null)
            {
                return new FileManagerResult<bool>(result.InternalException);
            }
            else if (!result.Result)
            {
                return new FileManagerResult<bool>(new InvalidOperationException("File not found!"));
            }

            var fileResult = InternalFileManager.RemoveFile(fileName);
            if (fileResult.InternalException != null)
            {
                return new FileManagerResult<bool>(fileResult.InternalException);
            }

            return new FileManagerResult<bool>(true);
        }
    }
}
