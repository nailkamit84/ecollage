﻿using ECollage.Files.Concrete;
using ECollage.Files.Management;
using ECollage.Files.Management.SpecialFiles;
using ECollage.Localization.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace ECollage.WebApi
{
    public static class HelperMethod
    {
        public static string GetSHAPassword(string password)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] sourceBytes = Encoding.UTF8.GetBytes(password);
                byte[] hashBytes = sha256Hash.ComputeHash(sourceBytes);
                string hash = BitConverter.ToString(hashBytes).Replace("-", String.Empty);
                return hash;
            }
        }

        public static string SaveFile(MultipartMemoryStreamProvider streamProvider, FileType fileType)
        {
            IEnumerable<HttpContent> streamInfo = streamProvider.Contents.Where(T => T.Headers.ContentDisposition.Name != "\"Id\"");
            var id = streamProvider.Contents.AsEnumerable().FirstOrDefault(T => T.Headers.ContentDisposition.Name == "\"Id\"").ReadAsStringAsync();
            if (streamInfo.Count() != 0 && !string.IsNullOrEmpty(id.Result))
            {
                foreach (HttpContent ctnt in streamInfo)
                {
                    Stream stream = ctnt.ReadAsStreamAsync().Result;
                    var _filename = ctnt.Headers.ContentDisposition.FileName;
                    var dotIndex = _filename.LastIndexOf(".");
                    var filename = _filename.Substring(0, dotIndex).Remove(0, 1);
                    var extention = _filename.Remove(_filename.Length - 1).Substring(dotIndex + 1);

                    var fileManager = new EColFileManager();
                    var newFile = new FileManagerClientAttachmentWithContent(stream, new FileManagerClientAttachment(GetAttachmentType(fileType), extention));
                    Files.Concrete.FileManagerResult<FileManagerResultEnum> result = fileManager.SaveClientAttachment(Convert.ToInt64(id.Result), newFile, fileType);
                    if (result.InternalException != null && result.Result == FileManagerResultEnum.Error)
                    {
                        //LOG EKLE
                        //loggerError.Fatal(result.InternalException, "CustomerSaveFile");
                        return "İç Sistem Hatası";
                    }
                }
                return "Başarılı";
            }
            return "Dosya Bulunamadı";
        }

        public static FileManagerResult<FileManagerClientAttachmentWithContent> GetFile(long Id, FileType fileType)
        {
            return GetFileInfo(Id, fileType);
        }
        public static bool EnumDefinedList(Type enumType, IEnumerable<int> value)
        {
            foreach (var item in value)
            {
                if (!Enum.IsDefined(enumType, item))
                {
                    return false;
                }
            }
            return true;
        }

        public static string UpdateFile(MultipartMemoryStreamProvider streamProvider, FileType fileType)
        {
            IEnumerable<HttpContent> streamInfo = streamProvider.Contents.Where(T => T.Headers.ContentDisposition.Name != "\"Id\"");
            var id = streamProvider.Contents.AsEnumerable().FirstOrDefault(T => T.Headers.ContentDisposition.Name == "\"Id\"").ReadAsStringAsync();

            if (streamInfo.Count() != 0 && !string.IsNullOrEmpty(id.Result))
            {
                foreach (HttpContent ctnt in streamInfo)
                {
                    Stream stream = ctnt.ReadAsStreamAsync().Result;
                    var _filename = ctnt.Headers.ContentDisposition.FileName;
                    var dotIndex = _filename.LastIndexOf(".");
                    var filename = _filename.Substring(0, dotIndex).Remove(0, 1);
                    var extention = _filename.Remove(_filename.Length - 1).Substring(dotIndex + 1);

                    var fileManager = new EColFileManager();

                    var removeFileInfo = GetFileInfo(Convert.ToInt64(id.Result), fileType);
                    if (removeFileInfo.Result != null && removeFileInfo.InternalException == null)
                    {
                        var removeFile = fileManager.RemoveClientAttachment(Convert.ToInt64(id.Result), removeFileInfo.Result.FileDetail.ServerSideName, fileType);
                        if (removeFile.InternalException != null || removeFile.Result == false)
                        {
                            return "Mevcut Belge Kaldırılırken Hata Oluştu";
                        }
                        var newFile = new FileManagerClientAttachmentWithContent(stream, new FileManagerClientAttachment(GetAttachmentType(fileType), extention));
                        FileManagerResult<FileManagerResultEnum> result = fileManager.SaveClientAttachment(Convert.ToInt64(id.Result), newFile, fileType);
                        if (result.InternalException != null && result.Result == FileManagerResultEnum.Error)
                        {
                            return "İç Sistem Hatası";
                        }
                    }
                    else
                    {
                        return "Mevcut Belge Bulunamadı";
                    }
                }
                return "Başarılı";
            }
            return "Dosya Bulunamadı";
        }

        private static FileManagerResult<FileManagerClientAttachmentWithContent> GetFileInfo(long Id, FileType fileType)
        {
            var fileManager = new EColFileManager();
            var resultAttachmentList = fileManager.GetClientAttachmentsList(Id, fileType);
            if (resultAttachmentList.InternalException != null)
            {
                return null;
            }
            FileManagerResult<FileManagerClientAttachmentWithContent> result = fileManager.GetClientAttachment(Id, resultAttachmentList.Result.OrderByDescending(r => r.CreationDate).Where(r => r.ServerSideName.Contains(GetAttachmentType(fileType).ToString())).FirstOrDefault()?.ServerSideName, fileType);
            if (result.InternalException != null)
            {
                return null;
            }
            return result;
        }

        public static short GetLang(HttpRequestHeaders httpRequestHeader)
        {
            if (httpRequestHeader.Contains("Accept-Language"))
            {
                if (httpRequestHeader.GetValues("Accept-Language").First() == "tr-TR")
                {
                    return (short)Country.TR;
                }
                else
                {
                    return (short)Country.GER;
                }
            }
            return (short)Country.TR;
        }

        private static ClientAttachmentTypes GetAttachmentType(FileType fileType)
        {
            switch (fileType)
            {
                case FileType.Product:
                    return ClientAttachmentTypes.Product;
                default:
                    return ClientAttachmentTypes.Others;
            }
        }
        public static IEnumerable<IEnumerable<T>> CartesianProduct<T>
      (this IEnumerable<IEnumerable<T>> sequences)
        {
            IEnumerable<IEnumerable<T>> emptyProduct =
              new[] { Enumerable.Empty<T>() };
            return sequences.Aggregate(
              emptyProduct,
              (accumulator, sequence) =>
                from accseq in accumulator
                from item in sequence
                select accseq.Concat(new[] { item }));
        }
    }
}