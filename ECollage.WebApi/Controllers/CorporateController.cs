﻿using ECollage.WebApi.AttributeECollage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ECollage.ViewModel.Corporate.Request;
using ECollage.Database.Model;

namespace ECollage.WebApi.Controllers
{
    [Exception]
    //[Authorize]
    public class CorporateController : ApiController
    {
        //[ValidateModel]
        //[HttpPost]
        ////[Authorize(Roles = "/corporate")]
        //public IHttpActionResult AddCorporateCustomer(AddCorporateViewModel model)
        //{
        //    using (var db = new EColEntities())
        //    {
        //        var user = db.User.FirstOrDefault(n => n.Username == model.UserInfo.Username);
        //        if (user == null)
        //        {
        //            if (model.AddedId.HasValue && db.User.FirstOrDefault(n => n.ID == model.AddedId.Value) == null)
        //            {
        //                return BadRequest("Ekleyen Kullanıcı Bilgileri Bulunamadı!");
        //            }
        //            var corporate = new CorporateCustomer
        //            {
        //                CompanyName = model.CompanyName,
        //                TaxNo = model.TaxNo,
        //                TaxOffice = model.TaxOffice,
        //                AddedID = model.AddedId,
        //                User = new User
        //                {
        //                    Name = model.UserInfo.Name,
        //                    Surname = model.UserInfo.Surname,
        //                    TCIdentitiy = model.UserInfo.TCIdentitiy,
        //                    BirthDate = model.UserInfo.BirthDate,
        //                    Email = model.UserInfo.Email,
        //                    Username = model.UserInfo.Username,
        //                    Password = HelperMethod.GetSHAPassword(model.UserInfo.Password),
        //                    IsActive = true,
        //                    Score = model.UserInfo.Score,
        //                    PhoneNumber = model.UserInfo.PhoneNumber,
        //                    CreatedDate = DateTime.Now,
        //                    UpdatedDate = DateTime.Now,
        //                    Address = model.AddressInfo.Select(ai => new Address
        //                    {
        //                        AddressName = ai.AddressName,
        //                        AddressText = ai.AddressText,
        //                        AddressType = ai.AddressType,
        //                        NeigborhoodID = ai.NeigborhoodID,
        //                    }).ToArray()
        //                },
        //            };
        //            db.CorporateCustomer.Add(corporate);
        //            db.SaveChanges();
        //            return Ok("Başarılı");
        //        }
        //        return BadRequest("Bu Kullanıcı Adı Kullanılıyor");
        //    }
        //}

        [ValidateModel]
        [HttpPost]
        // [Authorize(Roles = "/corporate")]
        public IHttpActionResult RemoveCorporate(RemoveCorporateViewModel model)
        {
            using (var db = new EColEntities())
            {
                var corporate = db.User.FirstOrDefault(c => c.ID == model.CorporateID);
                if (corporate != null)
                {
                    corporate.IsActive = false;
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("Tedarikçi Bilgileri Bulunamadı");
            }
        }

        [ValidateModel]
        [HttpPost]
        //[Authorize(Roles = "/corporate")]
        public IHttpActionResult UpdateCorporate(UpdateCorporateListViewModel model)
        {
            using (var db = new EColEntities())
            {
                var corporate = db.User.FirstOrDefault(c => c.ID == model.ID);
                if (corporate != null)
                {
                    //corporate.CompanyName = model.Name;
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("Kategori Bilgileri Bulunamadı");
            }
        }
    }
}
