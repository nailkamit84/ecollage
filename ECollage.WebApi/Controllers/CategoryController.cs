﻿using ECollage.Database.Model;
using ECollage.Localization.Enum;
using ECollage.ViewModel.Category;
using ECollage.ViewModel.Category.Request;
using ECollage.ViewModel.Category.Response;
using ECollage.WebApi.AttributeECollage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Globalization;
using System.Diagnostics;
using Country = ECollage.Localization.Enum.Country;
using ECollage.Localization;

namespace ECollage.WebApi.Controllers
{
    [Exception]
    //[Authorize]
    public class CategoryController : ApiController
    {
        [ValidateModel]
        [HttpPost]
        //[Authorize(Roles = "/category")]
        [AllowAnonymous]
        public IHttpActionResult AddCategory(AddCategoryViewModel[] addedModel)
        {
            using (var db = new EColEntities())
            {

                //if (Enum.IsDefined(typeof(CategoryMenu), addedModel.Menu))
                //{
                string categoryCode = $"{LocalizationType.Category}_{db.Localization.Count(l => l.Type == (short)LocalizationType.Category && l.Lang == (short)Localization.Enum.Country.TR)}";
                foreach (var item in addedModel)
                {
                    Database.Model.Localization localization = new Database.Model.Localization
                    {
                        Code = categoryCode,
                        Lang = (short)item.Lang,
                        Value = item.CategoryName,
                        Type = (short)LocalizationType.Category,
                    };
                    db.Localization.Add(localization);
                }
                var category = new Category
                {
                    CategoryName = categoryCode,
                    UpdatedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    Menu = addedModel.FirstOrDefault().Menu,
                    IsActive = true,
                    SequenceNo = GetCategorySequenceNo(),
                };
                db.Category.Add(category);
                db.SaveChanges();
                return Ok("Başarılı");
            }
            //}
            //return BadRequest("Girelen Menu Tanımsız");
        }
        [HttpPost]
        [Authorize(Roles = "/category")]
        [ValidateMimeMultipartContentFilter]
        public async Task<IHttpActionResult> AddCategoryImage()
        {
            var streamProvider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(streamProvider);
            return Ok(HelperMethod.SaveFile(streamProvider, FileType.Category));
        }
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetCategoryImage(long categoryID)
        {
            var file = HelperMethod.GetFile(categoryID, FileType.Category);
            HttpResponseMessage fullResponse = Request.CreateResponse(HttpStatusCode.OK);
            fullResponse.Content = new StreamContent(file.Result.Content);
            fullResponse.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(file.Result.FileDetail.MIMEType);
            return fullResponse;
        }

        [HttpPost]
        //[Authorize(Roles = "/product")]
        [ValidateMimeMultipartContentFilter]
        public async Task<IHttpActionResult> UpdateCategoryFile()
        {
            var streamProvider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(streamProvider);
            return Ok(HelperMethod.UpdateFile(streamProvider, FileType.Category));
        }

        private int GetCategorySequenceNo()
        {
            using (var db = new EColEntities())
            {
                return db.Category.Where(c => c.IsActive).Count() + 1;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetCategoryListWithCount()
        {
            using (var db = new EColEntities())
            {
                var categoryList = db.Category.Include(c => c.Product).Where(c => c.IsActive).OrderBy(c => c.SequenceNo).Select(c => new GetCategoryListWithCountResponse
                {
                    CategoryName = c.CategoryName,
                    ID = c.ID,
                    Count = c.Product.Count()
                }).ToList();
                return Ok(categoryList);
            }
        }
        [HttpGet]
        public IHttpActionResult GetCategoryList(int lang)
        {
            using (var db = new EColEntities())
            {
                var categoryList = db.Category.Where(c => c.IsActive).OrderBy(c => c.SequenceNo).Select(c => new GetCategoryListResponseViewModel
                {
                    ID = c.ID,
                    CategoryName = db.Localization.FirstOrDefault(l => l.Code == c.CategoryName && l.Lang == lang).Value,
                    CreatedDate = c.CreatedDate,
                    UpdatedDate = c.UpdatedDate
                }).ToList();
                return Ok(categoryList);
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetCategoryListWithProduct()
        {
            using (var db = new EColEntities())
            {
                var lang = HelperMethod.GetLang(Request.Headers);
                var localization = db.Localization.Where(l => l.Lang == lang).ToList();

                var categoryList = db.Category.Include(c => c.Product).Where(c => c.IsActive).AsEnumerable().GroupBy(d => d.Menu, d => d, (key, list) => new { menu = key, Liste = list.OrderBy(a => a.SequenceNo).ToList() })
                    .Select(c => new GetCategoryWithProductResponse
                    {
                        MenuID = ((CategoryMenu)c.menu).GetDescription(),
                        GetCategoryProduct = c.Liste.Select(cp => new GetCategoryProduct
                        {
                            CategoryName = localization.FirstOrDefault(l => l.Code == cp.CategoryName).Value,
                            ID = cp.ID,
                            GetProductResponseViewModel = cp.Product.Where(p => p.IsActive).OrderByDescending(p => p.SequenceNo).Select(p => new GetProductResponseViewModel
                            {
                                Code = p.Code,
                                ProductName = localization.FirstOrDefault(l => l.Code == p.Name).Value,
                                Description = localization.FirstOrDefault(l => l.Code == p.Description).Value,
                                ProductID = p.ID
                            })
                        })
                    }).ToList();
                //return BadRequest(Localization.Resorce.Response.NotFoundCategory);
                return Ok(categoryList);
            }
        }

        [ValidateModel]
        [HttpPost]
        [Authorize(Roles = "/category")]
        public IHttpActionResult RemoveCategory(RemoveCategoryRequestViewModel model)
        {
            using (var db = new EColEntities())
            {
                var category = db.Category.FirstOrDefault(c => c.ID == model.CategoryID);
                if (category != null)
                {
                    if (category.Product != null)
                    {
                        category.IsActive = false;
                        db.SaveChanges();
                        return Ok("Başarılı");
                    }
                    return BadRequest("Kategoride Ürünler Mevcuttur. Silinemez !");
                }
                return BadRequest("Kategori Bilgileri Bulunamadı");
            }
        }

        [ValidateModel]
        [HttpPost]
        [Authorize(Roles = "/category")]
        public IHttpActionResult UpdateCategory(UdpateCategoryRequestViewModel model)
        {
            using (var db = new EColEntities())
            {
                var category = db.Category.FirstOrDefault(c => c.ID == model.ID);
                if (category != null)
                {
                    category.CategoryName = model.Name;
                    category.UpdatedDate = DateTime.Now;
                    category.Menu = model.Menu;
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("Kategori Bilgileri Bulunamadı");
            }
        }

        [ValidateModel]
        [HttpPost]
        [Authorize(Roles = "/category")]
        public IHttpActionResult UpdateCategorySequence(UpdateCategorySequenceRequestVM model)
        {
            using (var db = new EColEntities())
            {
                var categoryList = db.Category.Where(dc => dc.IsActive).Select(dc => dc.ID).OrderBy(c => c).ToList();
                if (categoryList.SequenceEqual(model.CategoryIDList.OrderBy(c => c)))
                {
                    for (int i = 0; i < model.CategoryIDList.Length; i++)
                    {
                        var category = db.Category.Find(model.CategoryIDList[i]);
                        category.SequenceNo = (i + 1);
                        category.UpdatedDate = DateTime.Now;
                    }
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("Gönderilen Liste Uyumsuz!");
            }
        }
    }
}
