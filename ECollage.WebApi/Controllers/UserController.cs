﻿using ECollage.Database.Model;
using ECollage.ViewModel.User.Response;
using ECollage.WebApi.AttributeECollage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using ECollage.ViewModel.User.Request;
using ECollage.ViewModel.Corporate.Request;
using System.Data.Entity.Validation;

namespace ECollage.WebApi.Controllers
{
    [Exception]
    public class UserController : ApiController
    {
        [ValidateModel]
        [HttpPost]
        public IHttpActionResult GetUserInfo(GetUserInfoRequestViewModel model)
        {
            using (var db = new EColEntities())
            {
                var hashedPassword = HelperMethod.GetSHAPassword(model.Password);

                var userInfo = db.User.Where(u => u.Username == model.Username && u.Password == hashedPassword && u.IsActive).Include(u => u.UserRole).Include(u => u.UserRole.Select(ur => ur.Role)).
                    Include(u => u.UserRole.Select(ur => ur.Role.RolePermission)).Include(u => u.UserRole.Select(ur => ur.Role.RolePermission.Select(rp => rp.Permission))).
                    Select(u => new GetUserInfoResponseViewModel
                    {
                        UserID = u.ID,
                        Roles = u.UserRole.Select(ur => ur.Role.RoleName),
                        Permissions = u.UserRole.SelectMany(ur => ur.Role.RolePermission.Select(rp => rp.Permission.PermissionCode))
                    }).ToArray();

                return Ok(userInfo);
            }
        }

        private List<GetUserListResponseViewModel> GetUserList(GetUserListRequestViewModel model, Localization.Enum.Role role)
        {
            using (var db = new EColEntities())
            {
                var user = db.User.Where(u => u.IsActive).Include(u => u.UserRole).Include(u => u.UserRole.Select(ur => ur.Role)).Include(u => u.Address).Include(a => a.Address.Select(b => b.District))
                    .Include(a => a.Address.Select(b => b.District.Province))
                    .Include(a => a.Address.Select(b => b.District.Province.Country))
                    .Include(u => u.CorporateCustomer).AsQueryable();
                if (model != null && model.ID.HasValue)
                {
                    user = user.Where(u => u.ID == model.ID);
                }
                return user.Where(u => u.UserRole.Select(ur => ur.RoleID).Contains((int)role)).AsEnumerable().Select(u => new GetUserListResponseViewModel
                {
                    ID = u.ID,
                    Name = u.Name,
                    Username = u.Username,
                    BirthDate = u.BirthDate,
                    CreatedDate = u.CreatedDate,
                    Email = u.Email,
                    PhoneNumber = u.PhoneNumber,
                    Surname = u.Surname,
                    UpdatedDate = u.UpdatedDate,
                    AddressInfo = u.UserRole.Any(ur => ur.RoleID == (int)Localization.Enum.Role.Tedarikci) ? u.Address.Select(a => new AddressViewModel
                    {
                        AddressName = a.AddressName,
                        AddressText = a.AddressText,
                        AddressType = a.AddressType,
                        PostalCode = a.PostalCode,
                        Country = a.District.Province.Country.CountryName,
                        Province = a.District.Province.ProvinceName,
                        District = a.District.DistrictName,
                        CountryID = a.District.Province.Country.ID,
                        DistrictID = a.District.ID,
                        ProvinceID = a.District.Province.ID,
                    }) : null,
                    CorporateCustomerInfo = u.UserRole.Any(ur => ur.RoleID == (int)Localization.Enum.Role.Tedarikci) ? new CorporateCustomerViewModel
                    {
                        CompanyName = u.CorporateCustomer.CompanyName,
                        TaxNo = u.CorporateCustomer.CompanyName,
                        TaxOffice = u.CorporateCustomer.TaxOffice,
                    } : null
                }).ToList();
            }
        }

        [ValidateModel]
        [HttpPost]
        public IHttpActionResult UpdateSupplier(UpdateUserRequestViewModel updatedModel)
        {
            var updateUser = UpdateUser(updatedModel, Localization.Enum.Role.Tedarikci);
            if (updateUser.Key)
            {
                return Ok(updateUser.Value);
            }
            return BadRequest(updateUser.Value);

        }

        [ValidateModel]
        [HttpPost]
        public IHttpActionResult UpdateDesigner(UpdateUserRequestViewModel updatedModel)
        {
            var updateUser = UpdateUser(updatedModel, Localization.Enum.Role.Grafiker);
            if (updateUser.Key)
            {
                return Ok(updateUser.Value);
            }
            return BadRequest(updateUser.Value);

        }

        [ValidateModel]
        [HttpPost]
        //[Authorize(Roles = "/corporate")]
        public IHttpActionResult AddSupplier(AddUserViewModel model)
        {
            var addUser = AddUser(model, Localization.Enum.Role.Tedarikci);
            if (addUser.Key)
            {
                return Ok(addUser.Value);
            }
            return BadRequest(addUser.Value);
        }

        [ValidateModel]
        [HttpPost]
        //[Authorize(Roles = "/corporate")]
        public IHttpActionResult AddDesigner(AddUserViewModel model)
        {
            var addUser = AddUser(model, Localization.Enum.Role.Grafiker);
            if (addUser.Key)
            {
                return Ok(addUser.Value);
            }
            return BadRequest(addUser.Value);
        }

        [HttpPost]
        public IHttpActionResult GetDesignerInfo(GetUserListRequestViewModel model)
        {
            return Ok(GetUserList(model, Localization.Enum.Role.Grafiker));
        }

        [HttpPost]
        public IHttpActionResult GetSupplierInfo(GetUserListRequestViewModel model)
        {
            return Ok(GetUserList(model, Localization.Enum.Role.Tedarikci));
        }

        [ValidateModel]
        [HttpPost]
        public IHttpActionResult RemoveUser(RemoveUserViewModel model)
        {
            using (var db = new EColEntities())
            {
                var user = db.User.FirstOrDefault(c => c.ID == model.ID);
                if (user != null)
                {
                    user.IsActive = false;
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("Kullanıcı Bilgileri Bulunamadı");
            }
        }

        private KeyValuePair<bool, string> AddUser(AddUserViewModel model, Localization.Enum.Role role)
        {
            using (var db = new EColEntities())
            {
                var user = db.User.FirstOrDefault(n => n.Username == model.Username);
                if (user == null)
                {
                    if (model.AddedId.HasValue && db.User.FirstOrDefault(n => n.ID == model.AddedId.Value) == null)
                    {
                        return new KeyValuePair<bool, string>(false, "Ekleyen Kullanıcı Bilgileri Bulunamadı!");
                    }
                    if (role == Localization.Enum.Role.Tedarikci && (model.AddressInfo == null && model.CorporateInfo == null))
                    {
                        return new KeyValuePair<bool, string>(false, "Adres ve Şirket Bilgileribi Giriniz!");
                    }
                    if (role != Localization.Enum.Role.Tedarikci && string.IsNullOrEmpty(model.TCidentity))
                    {
                        return new KeyValuePair<bool, string>(false, "TC Kimlik Numarası Boş geçilemez!");
                    }
                    var addedUser = new User
                    {
                        Name = model.Name,
                        Surname = model.Surname,
                        TCIdentitiy = model.TCidentity,
                        BirthDate = model.BirthDate,
                        Email = model.Email,
                        Username = model.Username,
                        Password = HelperMethod.GetSHAPassword(model.Password),
                        IsActive = true,
                        Score = model.Score,
                        PhoneNumber = model.PhoneNumber,
                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now,
                        Address = role == Localization.Enum.Role.Tedarikci ? model.AddressInfo.Select(ai => new Address
                        {
                            AddressName = ai.AddressName,
                            AddressText = ai.AddressText,
                            AddressType = ai.AddressType,
                            PostalCode = ai.PostalCode,
                            DistrictID = ai.DistrictID,
                        }).ToArray() : null,
                        CorporateCustomer = role == Localization.Enum.Role.Tedarikci ? new CorporateCustomer
                        {
                            CompanyName = model.CorporateInfo.CompanyName,
                            TaxNo = model.CorporateInfo.TaxNo,
                            TaxOffice = model.CorporateInfo.TaxOffice,
                            AddedID = model.AddedId,
                        } : null,
                        UserRole = new List<UserRole> { new UserRole { RoleID = (int)role } }.ToArray(),
                    };
                    db.User.Add(addedUser);
                    db.SaveChanges();
                    return new KeyValuePair<bool, string>(true, "Başarılı");
                }
                return new KeyValuePair<bool, string>(false, "Bu Kullanıcı Adı Kullanılıyor");
            }

        }

        private KeyValuePair<bool, string> UpdateUser(UpdateUserRequestViewModel updatedModel, Localization.Enum.Role role)
        {
            using (var db = new EColEntities())
            {
                var user = db.User.Include(u => u.UserRole).Include(u => u.UserRole.Select(ur => ur.Role)).Include(u => u.Address).Include(u => u.CorporateCustomer).FirstOrDefault(u => u.ID == updatedModel.UserID && u.IsActive);
                if (user != null)
                {
                    if (updatedModel.AddedId.HasValue && db.User.FirstOrDefault(n => n.ID == updatedModel.AddedId.Value) == null)
                    {
                        return new KeyValuePair<bool, string>(false, "Ekleyen Kullanıcı Bilgileri Bulunamadı!");
                    }
                    if (role != Localization.Enum.Role.Tedarikci && string.IsNullOrEmpty(updatedModel.TCidentity))
                    {
                        return new KeyValuePair<bool, string>(false, "TC Kimlik Numarası Boş geçilemez!");
                    }
                    if (role == Localization.Enum.Role.Tedarikci && (updatedModel.AddressInfo == null && updatedModel.CorporateInfo == null))
                    {
                        return new KeyValuePair<bool, string>(false, "Adres ve Şirket Bilgileribi Giriniz!");
                    }
                    db.Address.RemoveRange(user.Address);
                    user.Name = updatedModel.Name;
                    user.BirthDate = updatedModel.BirthDate;
                    user.Email = updatedModel.Email;
                    user.PhoneNumber = updatedModel.PhoneNumber;
                    user.Surname = updatedModel.Surname;
                    user.UpdatedDate = DateTime.Now;
                    if (role == Localization.Enum.Role.Tedarikci)
                    {
                        db.Address.AddRange(updatedModel.AddressInfo.Select(ai => new Address
                        {
                            AddressName = ai.AddressName,
                            AddressText = ai.AddressText,
                            AddressType = ai.AddressType,
                            PostalCode = ai.PostalCode,
                            DistrictID = ai.DistrictID
                        }));
                        user.CorporateCustomer.CompanyName = updatedModel.CorporateInfo.CompanyName;
                        user.CorporateCustomer.TaxOffice = updatedModel.CorporateInfo.TaxOffice;
                    }
                    db.SaveChanges();
                    return new KeyValuePair<bool, string>(true, "Başarılı");
                }
                return new KeyValuePair<bool, string>(false, "Kullanıcı Bulunamadı!");
            }
        }

    }
}
