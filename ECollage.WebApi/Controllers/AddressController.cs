﻿using ECollage.Database.Model;
using ECollage.ViewModel.Country.Request;
using ECollage.ViewModel.Country.Response;
using ECollage.WebApi.AttributeECollage;
using System.Linq;
using System.Web.Http;


namespace ECollage.WebApi.Controllers
{
    public class AddressController : ApiController
    {
        // GET: Address 
        
        [HttpGet]
        public IHttpActionResult GetCountryList()
        {
            using (var db = new EColEntities())
            {
                var countryList = db.Country.Select(u => new GetCountryListResponseViewModel
                {
                    CountryID = u.ID,
                    CountryName = u.CountryName
                }).ToArray();

                return Ok(countryList);
            }
        }


        [ValidateModel]
        [HttpPost]
        public IHttpActionResult GetProvinceList(GetProvinceListViewModel model)
        {
            using (var db = new EColEntities())
            {
                var countryList = db.Province.Where(u => u.CountryID == model.CountryID)
                    .Select(u => new GetProvinceListResponseViewModel            
                    {   
                        ProvinceID = u.ID,
                        CountryID = u.CountryID,
                        ProvinceName = u.ProvinceName               
                    }).ToArray(); 
                return Ok(countryList);
            }
        }


        [ValidateModel]
        [HttpPost]
        public IHttpActionResult GetDistrictList(GetDistrictListViewModel model)
        {
            using (var db = new EColEntities())
            {
                var countryList = db.District.Where(u => u.ProvinceID == model.ProvinceID)
                    .Select(u => new GetDistrictListResponseViewModel
                    {
                        DistrictID = u.ID,
                        ProvinceID = u.ProvinceID,
                        DistrictName = u.DistrictName
                    }).ToArray();
                return Ok(countryList);
            }
        }
    }
}