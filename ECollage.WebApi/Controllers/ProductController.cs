﻿using ECollage.Database.Model;
using ECollage.ViewModel.Product.Request;
using ECollage.ViewModel.Product.Resposne;
using ECollage.WebApi.AttributeECollage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using ECollage.Files.Management;
using ECollage.Files.Management.SpecialFiles;
using System.IO;
using ECollage.Localization.Enum;
using System.Web.Http.ValueProviders.Providers;
using System.Reflection;
using System.Security.Policy;
using System.Data.Entity.Infrastructure;
using System.Net.NetworkInformation;
using Newtonsoft.Json.Linq;

namespace ECollage.WebApi.Controllers
{
    [Exception]
    //[Authorize]
    public class ProductController : ApiController
    {
        [ValidateModel]
        [HttpPost]
        //[Authorize(Roles = "/product")]
        public IHttpActionResult AddProduct(AddProductRequestViewModel[] addedModel)
        {
            if (HelperMethod.EnumDefinedList(typeof(Localization.Enum.Country), addedModel.Select(m => m.Lang)))
            {

                using (var db = new EColEntities())
                {
                    var category = db.Category.Find(addedModel.FirstOrDefault().CategoryID);
                    var descriptionCode = $"{LocalizationType.ProductDescription}_{db.Localization.Count(l => l.Type == (short)LocalizationType.ProductDescription && l.Lang == (short)Localization.Enum.Country.TR) + 1}";
                    var propertyCount = db.Localization.Count(l => l.Type == (short)LocalizationType.PropertyName && l.Lang == (short)Localization.Enum.Country.TR) + 1;
                    var productCode = $"{LocalizationType.ProductName}_{db.Localization.Count(l => l.Type == (short)LocalizationType.ProductName && l.Lang == (short)Localization.Enum.Country.TR) + 1}";
                    var propertyValueCodeCount = db.Localization.Count(l => l.Type == (short)LocalizationType.PropertyValue && l.Lang == (short)Localization.Enum.Country.TR) + 1;
                    foreach (var item in addedModel)
                    {
                        var list = new List<Database.Model.Localization>()
                    {
                        new Database.Model.Localization
                        {
                            Code = productCode,
                            Lang = (short)item.Lang,
                            Type = (short)LocalizationType.ProductName,
                            Value = item.Name
                        },
                        new Database.Model.Localization{
                            Code = descriptionCode,
                             Lang = (short)item.Lang,
                            Type = (short)LocalizationType.ProductDescription,
                            Value = item.Description
                        },
                     };

                        var properyName = item.ProductPropertyViewModel.Select((ppv, ss) => new Database.Model.Localization
                        {
                            Code = $"{LocalizationType.PropertyName}_{propertyCount + ss}",
                            Lang = (short)item.Lang,
                            Type = (short)LocalizationType.PropertyName,
                            Value = ppv.PropertyName
                        }).ToList();

                        var addedPropertyValue = item.ProductPropertyViewModel.SelectMany((ppv, a) => ppv.Value.Select((ab, b) => new Database.Model.Localization
                        {
                            Code = $"{LocalizationType.PropertyValue}_{propertyValueCodeCount + (a * item.ProductPropertyViewModel.Length + b)}",
                            Lang = (short)item.Lang,
                            Type = (short)LocalizationType.PropertyValue,
                            Value = ab
                        })).ToList();

                        list.AddRange(properyName);
                        list.AddRange(addedPropertyValue);

                        db.Localization.AddRange(list);
                    }
                    if (category != null)
                    {
                        var product = new Product
                        {
                            Code = addedModel.FirstOrDefault().Code,
                            Name = productCode,
                            CategoryID = addedModel.FirstOrDefault().CategoryID,
                            Description = descriptionCode,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            IsActive = true,
                            IsShare = addedModel.FirstOrDefault().IsShare,
                            SequenceNo = GetProductSequenceNo(),
                            ProductProperty = addedModel.FirstOrDefault().ProductPropertyViewModel.Select((ppvm, ss) => new ProductProperty
                            {
                                IsSelect = ppvm.IsSelect,
                                PropertyName = $"{LocalizationType.PropertyName}_{propertyCount + ss}",
                                PropertyTurn = ppvm.ProperyTurn,
                                ProductPropertyValue = ppvm.IsSelect == false ? null : ppvm.Value.Select((v, b) => new ProductPropertyValue { ValueName = $"{LocalizationType.PropertyValue}_{propertyValueCodeCount + (ss * addedModel.FirstOrDefault().ProductPropertyViewModel.Length + b)}" }).ToArray(),
                            }).ToArray(),
                        };
                        var addedProduct = db.Product.Add(product);

                        db.SaveChanges();

                        var cartesianProduct = CartesianProduct(addedProduct.ID);

                        foreach (var item in cartesianProduct)
                        {
                            var productPrice = new ProductPrice
                            {
                                PurchasePrice = 0,
                                SalePrice = 0,
                                StudentShare = 0,
                                DesignerShare = 0,
                                PurchaseKDV = 0,
                                SaleKDV = 0,
                                StudentStoppage = 0,
                                DesignerStoppage = 0,
                                Discount = 0,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                StudentGain = 0,
                                DesignerGain = 0,
                                PropertyGroup = item.Select(i => new PropertyGroup
                                {
                                    ProductPropertyValueID = i.ProductPropertyValue.Value,
                                }).ToArray()
                            };
                            db.ProductPrice.Add(productPrice);
                            db.SaveChanges();
                        }

                        return Ok(product.ID);
                    }
                    return BadRequest("Geçersiz Kategori");
                }
            }
            return BadRequest("Dil Seçeneği Doğrulanamadı!");
        }

        [ValidateModel]
        [HttpPost]
        //[Authorize(Roles = "/product")]
        public IHttpActionResult GetProductList(GetCategoryListRequestViewModel model)
        {
            using (var db = new EColEntities())
            {
                if (db.Category.Find(model.CategoryID) != null)
                {
                    var localization = db.Localization.Where(l => l.Lang == model.Lang).AsQueryable();
                    var productList = db.Product.Include(p => p.Category).Include(p => p.ProductProperty).Include(p => p.ProductProperty.Select(pp => pp.ProductPropertyValue))
                                       .Where(p => p.IsActive == true && p.CategoryID == model.CategoryID).OrderBy(p => p.SequenceNo).Select(p => new GetPropertyListViewModel
                                       {
                                           ID = p.ID,
                                           Code = p.Code,
                                           CategoryID = p.CategoryID,
                                           CategoryName = localization.FirstOrDefault(l => l.Code == p.Category.CategoryName).Value,
                                           Description = localization.FirstOrDefault(l => l.Code == p.Description).Value,
                                           Name = localization.FirstOrDefault(l => l.Code == p.Name).Value,
                                           CreatedDate = p.CreatedDate,
                                           UpdatedModel = p.UpdatedDate,
                                           IsShare = p.IsShare,
                                           ProductPropertyViewModel = p.ProductProperty.Select(pp => new ProductPropertyResponseViewModel
                                           {
                                               IsSelect = pp.IsSelect,
                                               PropertyName = localization.FirstOrDefault(l => l.Code == pp.PropertyName).Value,
                                               ProperyTurn = pp.PropertyTurn,
                                               Value = pp.ProductPropertyValue.Select(ppv => localization.FirstOrDefault(b => b.Code == ppv.ValueName).Value)
                                           })
                                       }).ToList();

                    return Ok(productList);
                }
                return BadRequest("Kategori Bilgisi Bulunamadı!");
            }
        }

        private List<List<ProductCartesionResponse>> CartesianProduct(int productID)
        {
            using (var db = new EColEntities())
            {
                var productPropertyDbList = new List<ProductCartesionResult>();
                var result = new List<ProductCartesionResponse>();
                var resultList = new List<List<ProductCartesionResponse>>();
                var productProperty = db.Product.Include(p => p.ProductProperty).Include(p => p.ProductProperty.Select(pp => pp.ProductPropertyValue)).FirstOrDefault(p => p.ID == productID).ProductProperty.ToArray();

                foreach (var item in productProperty)
                {
                    productPropertyDbList.Add(new ProductCartesionResult
                    {
                        ProductPropertyName = item.PropertyName,
                        ProductPropertyValueList = item.ProductPropertyValue.Select(ppv => new ProductPropertyValueList
                        {
                            Name = ppv.ValueName,
                            Value = ppv.ID
                        }).ToList()
                    });
                }

                var cartesianProductPropertyValue = HelperMethod.CartesianProduct(productPropertyDbList.Select(a => a.ProductPropertyValueList.Select(v => v.Value)));
                foreach (var item in cartesianProductPropertyValue)
                {
                    foreach (var item2 in item)
                    {
                        var listEquals = productPropertyDbList.FirstOrDefault(a => a.ProductPropertyValueList.Select(ppv => ppv.Value).Contains(item2));
                        //result.Clear();
                        var addedCartesionResult = new ProductCartesionResponse
                        {
                            ProductPropertyName = listEquals.ProductPropertyName,
                            ProductPropertyValue = new ProductPropertyValueList
                            {
                                Name = listEquals.ProductPropertyValueList.First(a => a.Value == item2).Name,
                                Value = item2
                            }
                        };
                        if (!result.Contains(addedCartesionResult))
                        {
                            result.Add(addedCartesionResult);
                        }
                    }
                    if (!resultList.Contains(result))
                    {
                        resultList.Add(result);
                        result = new List<ProductCartesionResponse>();
                    }
                }
                return resultList;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult CartesianProduct(CartesianProductRequestViewModel model)
        {
            using (var db = new EColEntities())
            {
                var product = db.Product.Include(p => p.ProductProperty).Include(p => p.ProductProperty.Select(pp => pp.ProductPropertyValue))
                    .Include(p => p.ProductProperty.Select(pp => pp.ProductPropertyValue.Select(s => s.PropertyGroup)))
                    .Include(p => p.ProductProperty.Select(pp => pp.ProductPropertyValue.Select(s => s.PropertyGroup.Select(a => a.ProductPrice))))
                    //.Include(p => p.ProductProperty.Select(pp => pp.ProductPropertyValue.Select(s => s.PropertyGroup)))
                    .FirstOrDefault(pr => pr.ID == model.ProductID);
                var localization = db.Localization.Where(l => l.Lang == model.Lang).AsQueryable();

                if (product != null)
                {
                    var productPriceKeyList = product.ProductProperty.SelectMany(pp => pp.ProductPropertyValue.SelectMany(ppv => ppv.PropertyGroup.GroupBy(pg => pg.GroupNo).Select(a => a.Key))).ToList();
                    var list = db.ProductPrice.Where(pp => productPriceKeyList.Contains(pp.ID)).Select(pgi => new CartesianProductResponseViewModel
                    {
                        CreatedDate = pgi.CreatedDate,
                        DesignerGain = pgi.DesignerGain,
                        DesignerShare = pgi.DesignerShare,
                        DesignerStoppage = pgi.DesignerStoppage,
                        Discount = pgi.Discount,
                        StudentStoppage = pgi.StudentStoppage,
                        PurchaseKDV = pgi.PurchaseKDV,
                        SaleKDV = pgi.SaleKDV,
                        ProductPriceID = pgi.ID,
                        PurchasePrice = pgi.PurchasePrice,
                        SalePrice = pgi.SalePrice,
                        StudentGain = pgi.StudentGain,
                        StudentShare = pgi.StudentShare,
                        UpdatedDate = pgi.UpdatedDate,
                        ProductCartesionResponse = pgi.PropertyGroup.AsEnumerable().Select(s => new ProductCartesionListResult
                        {
                            Name = localization.AsEnumerable().FirstOrDefault(l => l.Code == s.ProductPropertyValue.ValueName).Value,
                            Value = s.ProductPropertyValue.ID,
                            ProductPropertyName = localization.AsEnumerable().FirstOrDefault(l => l.Code == s.ProductPropertyValue.ProductProperty.PropertyName).Value
                        }).ToList()

                    }).ToList();
                    return Ok(list);
                }
                return BadRequest("Ürün Bulunamadı!");
            }
        }
        [AllowAnonymous]
        [ValidateModel]
        [HttpPost]
        public IHttpActionResult UpdateCartesianProduct(UpdateCartesianProductUpdateViewModel[] model)
        {
            using (var db = new EColEntities())
            {
                foreach (var item in model)
                {
                    var productPrice = db.ProductPrice.Find(item.ProductPriceID);
                    if (productPrice != null)
                    {
                        productPrice.PurchasePrice = item.PurchasePrice;
                        productPrice.SalePrice = item.SalePrice;
                        productPrice.StudentShare = item.StudentShare;
                        productPrice.DesignerShare = item.DesignerShare;
                        productPrice.SaleKDV = item.SaleKDV;
                        productPrice.PurchaseKDV = item.PurchaseKDV;
                        productPrice.StudentStoppage = item.StudentStoppage;
                        productPrice.DesignerStoppage = item.DesignerStoppage;
                        productPrice.Discount = item.Discount;
                        productPrice.StudentGain = item.StudentGain;
                        productPrice.DesignerGain = item.DesignerGain;
                    }
                    else
                    {
                        return BadRequest("Bilgi Bulunamadı!");
                    }
                }
                db.SaveChanges();
                return Ok("Başarılı");
            }
        }

        private int GetProductSequenceNo()
        {
            using (var db = new EColEntities())
            {
                return db.Product.Where(p => p.IsActive).Count() + 1;
            }
        }
        [ValidateModel]
        [HttpPost]
        public IHttpActionResult UpdateProductSequence(UpdateProductSequenceNoVM model)
        {
            using (var db = new EColEntities())
            {
                var productList = db.Product.Where(pr => pr.IsActive).Select(pr => pr.ID).OrderBy(c => c).ToList();
                if (productList.SequenceEqual(model.ProductID.OrderBy(c => c)))
                {
                    for (int i = 0; i < model.ProductID.Length; i++)
                    {
                        var product = db.Product.Find(model.ProductID[i]);
                        product.SequenceNo = (i + 1);
                        product.UpdatedDate = DateTime.Now;
                    }
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("GönderilenListe Uyumsuz!");
            }
        }

        [ValidateModel]
        [HttpPost]
        public IHttpActionResult UpdateProductList(UpdateProductListRequestVM model)
        {
            using (var db = new EColEntities())
            {
                var productList = db.Product.FirstOrDefault(pr => pr.ID == model.ProductID && pr.IsActive);
                var category = db.Category.FirstOrDefault(pr => pr.ID == model.UpdateModel.CategoryID && pr.IsActive);
                if (productList != null && category != null)
                {

                    db.ProductPropertyValue.RemoveRange(productList?.ProductProperty.SelectMany(p => p.ProductPropertyValue));
                    db.ProductProperty.RemoveRange(productList.ProductProperty);
                    db.ProductProperty.AddRange(model.UpdateModel.ProductPropertyViewModel.Select(p => new ProductProperty
                    {
                        IsSelect = p.IsSelect,
                        PropertyName = p.PropertyName,
                        ProductId = model.ProductID,
                        PropertyTurn = p.ProperyTurn,
                        ProductPropertyValue = p.Value.Select(a => new ProductPropertyValue
                        {
                            ValueName = a,
                            ProductPropertyID = model.ProductID
                        }).ToArray()
                    }));
                    productList.CategoryID = model.UpdateModel.CategoryID;
                    productList.Name = model.UpdateModel.Name;
                    productList.Description = model.UpdateModel.Description;
                    productList.UpdatedDate = DateTime.Now;
                    productList.IsShare = model.UpdateModel.IsShare;
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("Ürün veya Kategori Bilgisi Bulunamadı");
            }

        }

        [HttpPost]
        //[Authorize(Roles = "/product")]
        [ValidateMimeMultipartContentFilter]
        public async Task<IHttpActionResult> SaveProductFile()
        {
            var streamProvider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(streamProvider);
            return Ok(HelperMethod.SaveFile(streamProvider, FileType.Product));
        }

        [HttpPost]
        //[Authorize(Roles = "/product")]
        [ValidateMimeMultipartContentFilter]
        public async Task<IHttpActionResult> UpdateProductFile()
        {
            var streamProvider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(streamProvider);
            return Ok(HelperMethod.UpdateFile(streamProvider, FileType.Product));
        }

        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetProductImage(long id)
        {
            var file = HelperMethod.GetFile(id, FileType.Product);
            HttpResponseMessage fullResponse = Request.CreateResponse(HttpStatusCode.OK);
            fullResponse.Content = new StreamContent(file.Result.Content);
            fullResponse.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(file.Result.FileDetail.MIMEType);
            return fullResponse;
        }

        [HttpGet]
        public IHttpActionResult GetProductListMainPage()
        {
            using (var db = new EColEntities())
            {
                var lang = HelperMethod.GetLang(Request.Headers);

                var productList = db.Product.OrderByDescending(p => p.CreatedDate)
                .Select(p => new GetProductListMainPageViewModel
                {
                    ID = p.ID,
                    ProductName = db.Localization.FirstOrDefault(l => l.Lang == lang && l.Code == p.Name).Value,

                }).Take(8).ToArray();

                return Ok(productList);
            }
        }


        [ValidateModel]
        [HttpPost]
        public IHttpActionResult RemoveProduct(RemoveProductRequestViewModel model)
        {
            using (var db = new EColEntities())
            {
                var product = db.Product.FirstOrDefault(c => c.ID == model.ProductID);
                if (product != null)
                {
                    product.IsActive = false;
                    db.SaveChanges();
                    return Ok("Başarılı");
                }
                return BadRequest("Ürün Bilgileri Bulunamadı");
            }
        }

    }
}
