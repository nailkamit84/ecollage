﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace ECollage.WebApi.AttributeECollage
{
    public class ExceptionAttribute : ExceptionFilterAttribute
    {
        private static Logger loggerError = LogManager.GetLogger("AppLoggerError");
        public override void OnException(HttpActionExecutedContext context)
        {
            loggerError.Fatal(context.Exception, "Internal Error");

            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(context.Exception.Message),
                ReasonPhrase = context.Exception.Message
            });
        }
    }
}