﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECollage.WebApi.AttributeECollage
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CombinationsAttribute : Attribute
    {
        public object[] PossibleValues { get; private set; }
        public CombinationsAttribute(params object[] values)
        {
            this.PossibleValues = values;
        }
    }
}