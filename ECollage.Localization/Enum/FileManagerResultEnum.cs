﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Localization.Enum
{
    public enum FileManagerResultEnum
    {
        Success = 1,
        Error = 2,
        AlreadyExists = 3,
        NotFound = 4
    }
}
