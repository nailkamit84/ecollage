﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Localization.Enum
{
    public enum LocalizationType
    {
        Category = 1,
        ProductDescription = 2,
        PropertyName = 3,
        ProductName = 4,
        PropertyValue = 5,
    }
}
