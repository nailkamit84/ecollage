﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Localization.Enum
{
    public enum FileManagerTypes
    {
        Local = 1,
        Remote = 2
    }
}
