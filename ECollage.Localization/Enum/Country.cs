﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Localization.Enum
{
    public enum Country
    {
        [LocalizedDescription("TR", typeof(Resorce.Country))]
        TR = 1,
        [LocalizedDescription("GER", typeof(Resorce.Country))]
        GER = 2
    }
}
