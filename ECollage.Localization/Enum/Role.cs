﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Localization.Enum
{
    public enum Role
    {
        Admin = 1,
        Tedarikci = 2,
        Grafiker = 3,
    }
}
