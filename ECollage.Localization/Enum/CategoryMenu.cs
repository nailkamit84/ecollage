﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Localization.Enum
{
    public enum CategoryMenu
    {
        [LocalizedDescription("Matbaa", typeof(Resorce.CategoryMenu))]
        Matbaa = 1,
        [LocalizedDescription("Promosyon", typeof(Resorce.CategoryMenu))]
        Promosyon = 2,
        [LocalizedDescription("Reklam", typeof(Resorce.CategoryMenu))]
        Reklam = 3,
        [LocalizedDescription("OfisGerecleri", typeof(Resorce.CategoryMenu))]
        OfisGerecleri = 4,
        [LocalizedDescription("Covid19", typeof(Resorce.CategoryMenu))]
        Covid19 = 5
    }
}
