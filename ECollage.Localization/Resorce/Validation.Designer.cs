﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECollage.Localization.Resorce {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Validation {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Validation() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ECollage.Localization.Resorce.Validation", typeof(Validation).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Formatında Giriniz..
        /// </summary>
        public static string EmailFormat {
            get {
                return ResourceManager.GetString("EmailFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} 150 karakterden fazla olamaz..
        /// </summary>
        public static string Max150 {
            get {
                return ResourceManager.GetString("Max150", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} 20 karakterden fazla olamaz..
        /// </summary>
        public static string Max20 {
            get {
                return ResourceManager.GetString("Max20", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} 200 karakterden fazla olamaz..
        /// </summary>
        public static string Max200 {
            get {
                return ResourceManager.GetString("Max200", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} 250 karakterden fazla olamaz..
        /// </summary>
        public static string Max250 {
            get {
                return ResourceManager.GetString("Max250", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} 32 karakterden fazla olamaz..
        /// </summary>
        public static string Max32 {
            get {
                return ResourceManager.GetString("Max32", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} 8karakterden fazla olamaz..
        /// </summary>
        public static string Max8 {
            get {
                return ResourceManager.GetString("Max8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} Sadece sayısal karakter girebilirsiniz..
        /// </summary>
        public static string OnlyNumeric {
            get {
                return ResourceManager.GetString("OnlyNumeric", resourceCulture);
            }
        }
    }
}
