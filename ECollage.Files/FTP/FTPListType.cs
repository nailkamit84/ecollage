﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.Files.FTP
{
    public enum FTPListType
    {
        File = 1,
        Directory = 2
    }
}
