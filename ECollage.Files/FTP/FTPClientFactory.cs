﻿using System;

namespace ECollage.Files.FTP
{
    public static class FTPClientFactory
    {
        public static FTPClientBase CreateFTPClient(string url, string username, string password)
        {
            if (url.ToLower().StartsWith("ftp://"))
            {
                return new FTPClient(url, username, password);
            }
            else if (url.ToLower().StartsWith("sftp://"))
            {
                return new SFTP.SFTPClient(url, username, password);
            }

            throw new NotSupportedException("Url must start with ftp:// or sftp://");
        }
    }
}
