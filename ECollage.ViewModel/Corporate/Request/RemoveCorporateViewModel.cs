﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Corporate.Request
{
    public class RemoveCorporateViewModel
    {
        [Required(ErrorMessage = "Kategori Bilgilerini Giriniz")]
        public int CorporateID { get; set; }
    }
}
