﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Corporate.Request
{
    public class UpdateCorporateListViewModel
    {
        [Required(ErrorMessage = "Tedarikçi ID Boş")]
        public int ID { get; set; }
        [Required(ErrorMessage = "Tedarikçi Adı Boş")]
        public string Name { get; set; }
    }
}
