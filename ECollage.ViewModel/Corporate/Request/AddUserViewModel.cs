﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Corporate.Request
{
    public class AddUserViewModel
    {
        [Required]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string Name { get; set; }
        [Required]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string Surname { get; set; }

        [MaxLength(11, ErrorMessage = "Maksimum 11 karakter olmalı")]
        [MinLength(11, ErrorMessage = "Minimum 11 karakter olmalı")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "OnlyNumeric")]
        public string TCidentity { get; set; }
        public DateTime? BirthDate { get; set; }

        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((([a-zA-Z\-]+\.)+))([a-zA-Z]{2,4})(\]?)$", ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "EmailFormat")]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string Email { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "Minimum 6 karakter olmalı")]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string Username { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "Minimum 6 karakter olmalı")]
        public string Password { get; set; }
        public int? Score { get; set; }
        [Required]
        [MaxLength(20, ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "Max20")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "OnlyNumeric")]
        public string PhoneNumber { get; set; }

        public long? AddedId { get; set; }

        public AddressContext[] AddressInfo { get; set; }

        public CorporateContext CorporateInfo { get; set; }

    }

    public class AddressContext
    {
        [Required]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string AddressText { get; set; }

        public string AddressType { get; set; }

        public string AddressName { get; set; }

        public string PostalCode { get; set; }
        public int DistrictID { get; set; }
    }

    public class CorporateContext
    {
        [Required(ErrorMessage = "Kategori Bilgilerini Giriniz")]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string CompanyName { get; set; }

        [Required]
        [MaxLength(10, ErrorMessage = "Maksimum 10 karakter olmalı")]
        public string TaxNo { get; set; }
        [Required]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string TaxOffice { get; set; }
    }
}
