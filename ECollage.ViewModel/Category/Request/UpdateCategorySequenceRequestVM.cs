﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Category.Request
{
    public class UpdateCategorySequenceRequestVM
    {
        [Required(ErrorMessage = "Kategori ID List Boş")]
        public int[] CategoryIDList { get; set; }
    }
}
