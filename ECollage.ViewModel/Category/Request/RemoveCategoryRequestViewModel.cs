﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Category.Request
{
    public class RemoveCategoryRequestViewModel
    {
        [Required(ErrorMessage = "Kategori Bilgilerini Giriniz")]
        public int CategoryID { get; set; }
    }
}
