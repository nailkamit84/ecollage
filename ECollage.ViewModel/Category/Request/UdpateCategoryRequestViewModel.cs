﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Category.Request
{
   public class UdpateCategoryRequestViewModel
    {
        [Required(ErrorMessage ="Kategori ID Boş")]
        public int ID { get; set; }
        [Required(ErrorMessage ="Kategori Adı Boş")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Menü Bilgisi Boş")]
        public int Menu { get; set; }
    }
}
