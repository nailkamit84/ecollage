﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Category
{
    public class AddCategoryViewModel
    {
        [Required]
        public int Lang { get; set; }
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public int Menu { get; set; }
    }
}
