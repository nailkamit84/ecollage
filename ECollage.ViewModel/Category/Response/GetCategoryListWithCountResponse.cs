﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Category.Response
{
    public class GetCategoryListWithCountResponse
    {
        public int ID { get; set; }

        public string CategoryName { get; set; }

        public int Count { get; set; }
    }
}
