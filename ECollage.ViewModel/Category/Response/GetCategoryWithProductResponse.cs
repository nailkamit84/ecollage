﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Category.Response
{

    public class GetCategoryWithProductResponse
    {
        public string MenuID { get; set; }
        public IEnumerable<GetCategoryProduct> GetCategoryProduct { get; set; }
    }
    public class GetCategoryProduct
    {
        public int ID { get; set; }
        public string CategoryName { get; set; }
        public IEnumerable<GetProductResponseViewModel> GetProductResponseViewModel { get; set; }
    }
    public class GetProductResponseViewModel
    {
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }

    }

}
