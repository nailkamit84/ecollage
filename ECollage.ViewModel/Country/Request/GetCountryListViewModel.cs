﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Country.Request
{
    public class GetCountryListViewModel
    {
        [Required(ErrorMessage = "Ülke Bilgisi Boş!")]
        public int CountryID { get; set; }
    }
}
