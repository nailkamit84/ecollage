﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Country.Response
{
    public class GetProvinceListResponseViewModel
    {
        public long ProvinceID { get; set; }

        public long CountryID { get; set; }

        public string ProvinceName { get; set; }
    }
}
