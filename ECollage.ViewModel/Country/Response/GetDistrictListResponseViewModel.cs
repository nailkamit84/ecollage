﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Country.Response
{
    public class GetDistrictListResponseViewModel
    {
        public long DistrictID { get; set; }

        public long ProvinceID { get; set; }

        public string DistrictName { get; set; }
    }
}
