﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Resposne
{
    public class CartesianProductResponseViewModel
    {
        public int ProductPriceID { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal SalePrice { get; set; }
        public decimal? StudentShare { get; set; }
        public decimal? DesignerShare { get; set; }
        public decimal SaleKDV { get; set; }
        public decimal PurchaseKDV { get; set; }
        public decimal? StudentStoppage { get; set; }
        public decimal? DesignerStoppage { get; set; }
        public decimal? Discount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public decimal? StudentGain { get; set; }
        public decimal? DesignerGain { get; set; }
        public List<ProductCartesionListResult> ProductCartesionResponse { get; set; }
    }
}
