﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Resposne
{
    public class GetPropertyListViewModel
    {
        public int ID { get; set; }
        public bool IsShare { get; set; }
        public string Code { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedModel { get; set; }

        public string Description { get; set; }

        public int? Turn { get; set; }
        public IEnumerable<ProductPropertyResponseViewModel> ProductPropertyViewModel { get; set; }
    }

    public class ProductPropertyResponseViewModel
    {
        public string PropertyName { get; set; }

        public bool IsSelect { get; set; }

        public int? ProperyTurn { get; set; }

        public IEnumerable<string> Value { get; set; }

    }
}
