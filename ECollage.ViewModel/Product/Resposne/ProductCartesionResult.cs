﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Resposne
{
    public class ProductCartesionResult
    {
        public string ProductPropertyName { get; set; }

        public List<ProductPropertyValueList> ProductPropertyValueList { get; set; }
    }
    public class ProductCartesionListResult
    {
        public string ProductPropertyName { get; set; }
        public int Value { get; set; }
        public string Name { get; set; }
    }
    public class ProductPropertyValueList
    {
        public int Value { get; set; }
        public string Name { get; set; }
    }

    public class ProductCartesionResponse
    {
        public string ProductPropertyName { get; set; }

        public ProductPropertyValueList ProductPropertyValue{ get; set; }
    }
}
