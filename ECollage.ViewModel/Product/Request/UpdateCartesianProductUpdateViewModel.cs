﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Request
{
    public class UpdateCartesianProductUpdateViewModel
    {
        [Required]
        public int ProductPriceID { get; set; }
        [Required]
        public decimal PurchasePrice { get; set; }
        [Required]
        public decimal SalePrice { get; set; }
        public decimal? StudentShare { get; set; }
        public decimal? DesignerShare { get; set; }
        [Required]
        public decimal SaleKDV { get; set; }
        [Required]
        public decimal PurchaseKDV { get; set; }
        public decimal? StudentStoppage { get; set; }
        public decimal? DesignerStoppage { get; set; }
        public decimal? Discount { get; set; }
        public decimal ?StudentGain { get; set; }
        public decimal? DesignerGain { get; set; }
    }
}
