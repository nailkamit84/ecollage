﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Request
{
    public class AddProductRequestViewModel
    {
        [Required]
        public int Lang { get; set; }

        [MaxLength(150, ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "Max150")]
        public string Code { get; set; }

        [Required]
        [MaxLength(250, ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "Max250")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Yayınlansın Mı? Alanı Boş Bırakılamaz!")]
        public bool IsShare { get; set; }

        [Required]
        public int CategoryID { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public ProductPropertyViewModel[] ProductPropertyViewModel { get; set; }
    }

    public class ProductPropertyViewModel
    {
        [MaxLength(250, ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "Max250")]
        public string PropertyName { get; set; }

        public bool IsSelect { get; set; }

        public int? ProperyTurn { get; set; }

        [MaxLength(250, ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "Max250")]
        public string[] Value { get; set; }
    }
}


