﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Request
{
    public class UpdateProductSequenceNoVM
    {
        [Required(ErrorMessage = "Ürün ID Boş")]
        public int[] ProductID { get; set; }


    }
}
