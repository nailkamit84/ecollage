﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Request
{
    public class GetCategoryListRequestViewModel
    {
        [Required(ErrorMessage = "Kategori Bilgisi Boş!")]
        public int CategoryID { get; set; }
        [Required(ErrorMessage = "Dil Bilgisi Boş!")]
        public int Lang { get; set; }
    }
}
