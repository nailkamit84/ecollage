﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.Product.Request
{
    public class UpdateProductListRequestVM
    {
        public AddProductRequestViewModel UpdateModel { get; set; }
        public int ProductID { get; set; }
    }
}
