﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.User.Request
{
    public class GetUserListRequestViewModel
    {
        public long? ID { get; set; }
    }
}
