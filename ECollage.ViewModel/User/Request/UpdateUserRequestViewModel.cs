﻿using ECollage.ViewModel.Corporate.Request;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.User.Request
{
    public class UpdateUserRequestViewModel
    {
        [Required(ErrorMessage = "Kullanıcı ID Bilgisi Giriniz")]
        public long UserID { get; set; }

      
        [Required]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string Name { get; set; }
        [Required]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string Surname { get; set; }

        [MaxLength(11, ErrorMessage = "Maksimum 11 karakter olmalı")]
        [MinLength(11, ErrorMessage = "Minimum 11 karakter olmalı")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "OnlyNumeric")]
        public string TCidentity { get; set; }

        public DateTime BirthDate { get; set; }

        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((([a-zA-Z\-]+\.)+))([a-zA-Z]{2,4})(\]?)$", ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "EmailFormat")]
        [MaxLength(250, ErrorMessage = "Maksimum 250 karakter olmalı")]
        public string Email { get; set; }
  
        public int? Score { get; set; }
        [Required]
        [MaxLength(10, ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "Max20")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Localization.Resorce.Validation), ErrorMessageResourceName = "OnlyNumeric")]
        public string PhoneNumber { get; set; }

        public long? AddedId { get; set; }

        public AddressContext[] AddressInfo { get; set; }

        public CorporateContext CorporateInfo { get; set; }
        
    }
}
