﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.User.Request
{
    public class RemoveUserViewModel
    {
        [Required(ErrorMessage = "Bilgileri Giriniz")]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
