﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.User.Response
{
    public class GetUserListResponseViewModel
    {
        public long ID { get; set; }

        public string Name { get; set; }
        public string Username { get; set; }
        public string Surname { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public CorporateCustomerViewModel CorporateCustomerInfo { get; set; }

        public IEnumerable<AddressViewModel> AddressInfo { get; set; }
    }

    public class AddressViewModel
    {
        public string Province { get; set; }
        public int ProvinceID { get; set; }
        public string District { get; set; }
        public int DistrictID { get; set; }
        public string Country { get; set; }
        public int CountryID { get; set; }
        public string AddressText { get; set; }
        public string AddressType { get; set; }
        public string PostalCode { get; set; }
        public string AddressName { get; set; }
    }

    public class CorporateCustomerViewModel
    {
        public string CompanyName { get; set; }
        public string TaxNo { get; set; }
        public string TaxOffice { get; set; }
    }
}
