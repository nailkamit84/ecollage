﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECollage.ViewModel.User.Response
{
    public class GetUserInfoResponseViewModel
    {
        public long UserID { get; set; }

        public IEnumerable<string> Roles { get; set; }

        public IEnumerable<string> Permissions { get; set; }
    }
}
